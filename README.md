osm-tchoutchou
==============

An Open Street Map (leaflet) interface to Turblog's raildar.fr data.

View : http://raildar.fr

Some details on the original raildar map and API : 
* http://wiki.raildar.fr/index.php/Accueil
* https://pad.ilico.org/p/raildar
* http://blog.spyou.org/wordpress-mu/2013/11/30/jstchoutchou/


How to install :
----------------
Just clone the repository wherever you want on your web server. It's only static file now.


Licence :
---------
CC by-sa / GNU AGPL